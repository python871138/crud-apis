from fastapi import FastAPI
from routes.student_details import details_router

app = FastAPI()

app.include_router(details_router, tags=["Tags"], prefix="/api/v1")