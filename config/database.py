from pymongo.mongo_client import MongoClient

# this uri is my personal mongodb atlas cloud uri to do CRUD operations
# It helps to make direct API hits when server is up, No need to install MongoDB in local
uri = "mongodb+srv://admin:Admin@cluster0.ov7lhgl.mongodb.net/?retryWrites=true&w=majority&appName=AtlasApp"

client = MongoClient(uri)

db = client.students
