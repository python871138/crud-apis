from pydantic import BaseModel, Field


class PersonalDetails(BaseModel):
    """
    This model validates the create user details which comes from request body and adheres the REST principles
    """
    rollNo: int = Field(..., description="This value specify the roll number of a student", example=1)
    name: str = Field(..., min_length=3, description="This value specify the name of a student", example="hai")
    standard: int = Field(..., lt=11, gt=-1, description="This value specify the standard of a student", example=10)
    section: str = Field(..., max_length=1, min_length=1, description="This value specify the section of a student",
                         example="A")


class UpdateDetails(BaseModel):
    """
    This model validates the update user details which comes from request body and adheres the REST principles
    """
    name: str = Field(None, min_length=3, description="This value specify the name of a student", example="hai")
    standard: int = Field(None, lt=11, gt=-1, description="This value specify the standard of a student", example=10)
    section: str = Field(None, max_length=1, min_length=1, description="This value specify the section of a student",
                         example="A")
