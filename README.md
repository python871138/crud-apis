This project is about CRUD operations on student details.  
Prerequisite:     

> Python version 3.11  
> Any IDE (Prefer Pycharm)  

1.Clone the project  
2.Create virtual environment if you want  
3.Need to install  
> 1.fastapi  
  2.uvicorn  
  3.pymongo  
  4.pytest  
  5.httpx
>   
4.Run the command to start the server  
  
> uvicorn main:app --host 0.0.0.0 --port 80  
> 
5.Hit the below url to see the automatic interactive API documentation (list of APIs)  
> http://localhost:80/docs  
  80 - is default port for Fast api  
> 
6.Used MongoDB atlas cloud to store the data  
7.Execute the below command to run the test cases 
>  pytest -v  
> 
