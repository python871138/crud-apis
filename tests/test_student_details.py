from unittest.mock import patch
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

student_details = {
    "rollNo": 1,
    "name": "hai",
    "standard": 2,
    "section": "A"
}


@patch('routes.student_details.personal_info_db')
def test_fetch_student_details_case_1(personal_info_db_mock):
    """
    Test case to test the fetch students api
    for successful response
    """
    personal_info_db_mock.find_one.return_value = student_details
    response = client.get("/api/v1/get-details/rollNo?rollNo=3")
    response_json = response.json()
    assert response.status_code == 200
    assert response_json["message"] == "Student details are fetched successfully"
    assert response_json["data"] == student_details
    assert response_json["status"] is True


@patch('routes.student_details.personal_info_db')
def test_fetch_student_details_case_2(personal_info_db_mock):
    """
    Test case to test the fetch students api
    when there is no student
    """
    personal_info_db_mock.find_one.return_value = None
    response = client.get("/api/v1/get-details/rollNo?rollNo=3")
    response_json = response.json()
    assert response.status_code == 200
    assert response_json["message"] == "Student does not exist on roll number 3"
    assert response_json["data"] is None
    assert response_json["status"] is True


@patch('routes.student_details.personal_info_db')
def test_fetch_student_details_case_3(personal_info_db_mock):
    """
    Test case to test the fetch students api
    when exception occurs
    """
    personal_info_db_mock.find_one.side_effect = [Exception('database error')]
    response = client.get("/api/v1/get-details/rollNo?rollNo=3")
    response_json = response.json()
    assert response.status_code == 500
    assert response_json["message"] == "Failed to fetch student details due to error database error"
    assert response_json["data"] is None
    assert response_json["status"] is False


def test_fetch_student_details_case_4():
    """
    Test case to test the fetch students api
    where passing invalid path
    """
    response = client.get("/api/v1/geils/rollNo?rollNo=3")
    assert response.status_code == 404


def test_fetch_student_details_case_5():
    """
    Test case to test the fetch students api
    where passing invalid rollNo value
    """
    response = client.get("/api/v1/get-details/rollNo?rollNo=a")
    assert response.status_code == 422

