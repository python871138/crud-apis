from fastapi import APIRouter
from starlette.responses import JSONResponse
from config.database import db
from models.student_models import PersonalDetails, UpdateDetails

details_router = APIRouter()

# collection name is personal_info
personal_info_db = db["personal_info"]


@details_router.post('/add-details')
async def add_student_details(details: PersonalDetails):
    """
    API is using to store the student details
    :param details: request body
    :return: student details in json format
    """
    try:
        details_dict = details.model_dump()
        # Checking whether student is there or not on respective roll number
        student_data = personal_info_db.find_one({"rollNo": details_dict["rollNo"]})
        if not student_data:
            # If student is not there on respective roll number then add the student details
            personal_info_db.insert_one(details_dict)
            return JSONResponse(
                status_code=201,
                content={"message": 'Student details are added', "status": True, "data": details.model_dump()},
            )
        else:
            # If student is there on a respective roll number then return a message
            # Every student has unique roll number, It will not be duplicate
            return JSONResponse(
                status_code=200,
                content={"message": f'Student already exist on roll number {details_dict["rollNo"]}', "status": True,
                         "data": None},
            )
    except Exception as err:
        return JSONResponse(
            status_code=500,
            content={"message": f'Failed to add student details due to error {err}', "status": False,
                     "data": None},
        )


@details_router.get('/get-details/rollNo')
async def fetch_student_details(rollNo: int):
    """
    API using to display student details
    :param rollNo: integer
    :return: student details in json format
    """
    try:
        # Checking whether student is there or not on respective roll number
        response = personal_info_db.find_one({"rollNo": rollNo}, {"_id": 0})
        if response:
            # If student is there then return the response
            data_dict = {data: response[data] for data in response if data in ["rollNo", "name", "standard", "section"]}
            return JSONResponse(
                status_code=200,
                content={"message": 'Student details are fetched successfully', "status": True, "data": data_dict},
            )
        else:
            # If not return the message
            return JSONResponse(
                status_code=200,
                content={"message": f'Student does not exist on roll number {rollNo}', "status": True, "data": None},
            )
    except Exception as err:
        return JSONResponse(
            status_code=500,
            content={"message": f'Failed to fetch student details due to error {err}', "status": False,
                     "data": None},
        )


@details_router.put('/update-details/rollNo')
async def update_student_details(rollNo: int, update_details: UpdateDetails):
    """
    API using to update student details
    :param update_details: request body
    :param rollNo: integer
    :return: student details in json format
    """
    try:
        # Checking whether student is there or not on respective roll number
        response = personal_info_db.find_one({"rollNo": rollNo}, {"_id": 0})
        if response:
            update_details_dict = update_details.model_dump()
            # Checking the request body fields has values or not
            update_data = {data: update_details_dict[data] for data in update_details_dict if
                           update_details_dict[data] is not None}
            if update_data is None:
                # If empty request body comes
                return JSONResponse(
                    status_code=200,
                    content={"message": 'Invalid request body, At least need to update one filed', "status": True,
                             "data": None},
                )
            # Updating the student details
            response = personal_info_db.update_one({"rollNo": rollNo}, {"$set": update_data})
            if response.modified_count:
                # If there is a change in values of request body
                return JSONResponse(
                    status_code=200,
                    content={"message": 'Student details are updated successfully', "status": True,
                             "data": update_data},
                )
            else:
                # If there is no change in values, same as existing student document
                return JSONResponse(
                    status_code=200,
                    content={"message": 'Student details are upto date, Please provide new changes to update',
                             "status": True, "data": None},
                )
        else:
            # If student is not there on respective roll number to update
            return JSONResponse(
                status_code=200,
                content={"message": f'Student does not exist on roll number {rollNo}', "status": True, "data": None},
            )
    except Exception as err:
        return JSONResponse(
            status_code=500,
            content={"message": f'Failed to update student details due to error {err}', "status": False,
                     "data": None},
        )


@details_router.delete('/delete-details/rollNo')
async def delete_student_details(rollNo: int):
    """
    API using to delete student details
    :param rollNo: integer
    :return: student details in json format
    """
    try:
        # Checking whether student is there or not on respective roll number
        response = personal_info_db.find_one({"rollNo": rollNo}, {"_id": 0})
        if response:
            # If student is there then delete the student from the database
            personal_info_db.delete_one({"rollNo": rollNo})
            return JSONResponse(
                status_code=200,
                content={"message": 'Student details are deleted successfully', "status": True,
                         "data": {"rollNo": rollNo}},
            )
        else:
            # If student is not there then return a message
            return JSONResponse(
                status_code=200,
                content={"message": f'Student does not exist on roll number {rollNo}', "status": True, "data": None},
            )
    except Exception as err:
        return JSONResponse(
            status_code=500,
            content={"message": f'Failed to delete student details due to error {err}', "status": False,
                     "data": None},
        )
